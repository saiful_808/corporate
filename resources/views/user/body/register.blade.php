 @extends('user.layout.app')
@section('content')
<link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
 <section class="ftco-section contact-section ftco-degree-bg">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 text-center heading-section ftco-animate">
            <span class="subheading">Pendaftaran</span>
            <h2 class="mb-2">Lengkapi data dengan benar</h2>
          </div>
        </div>
        <div class="row block-9 justify-content-center">
          <div class="col-md-6 pr-md-5">
            <form action="/create_contract" method="post">
               {{csrf_field()}}
              <div class="form-group">
                <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama perusahaan" name="name" autocomplete="off">
                @error('name')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
              <div class="form-group">
                <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" autocomplete="off">
                 @error('email')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
                <div class="form-group">
                <input type="number" class="form-control @error('number') is-invalid @enderror" placeholder="Nomer Telepon" name="number" autocomplete="off">
                 @error('number')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
              <div class="form-group">
                <select class="form-control @error('service') is-invalid @enderror" name="contract">
                  <option>-- Tipe Kontrak --</option>
                  @foreach($data_contract as $num)
                    <option value="{{$num->id}}" class="form-control">{{$num->name}}</option>
                  @endforeach
                </select>
                @error('service')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group">
                <select class="form-control @error('service') is-invalid @enderror" name="service" id="kategori">
                  <option>-- Pilih Jenis Pelayanan --</option>
                  @foreach($data_service as $num)
                    <option value="{{$num->id}}" class="form-control">{{$num->name}}</option>
                  @endforeach
                </select>
                @error('service')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                @enderror
              </div>
               <div class="form-group">
                        <select name="sub_service" class="subkategori form-control">
                            <option value="0">-- Pilih Detail Pelayanan --</option>
                        </select>
                </div>
              <div class="form-group">
                <label>----- optional -----</label>
               <div class="input-group date" data-provide="datepicker">
                      <input type="text" class="form-control" placeholder="Tanggal Selesai Pengerjaan">
                      <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                <textarea id="" cols="30" rows="7" class="form-control @error('detail') is-invalid @enderror" placeholder="Detail projek" name="detail"></textarea>
                 @error('detail')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
              <div class="form-group">
                <input type="submit" value="Kirim" class="btn btn-primary py-3 px-5">
              </div>
            </form>
          
          </div>
        </div>
      </div>
    </section>
    @endsection