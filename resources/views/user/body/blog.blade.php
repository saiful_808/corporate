@extends('user.layout.app')
@section('content')
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row">
          @foreach($artikel as $value)
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style='background-image: url({{ asset("uploads/imagehd/thumbnail/$value->thumbnail")}})'>
              </a>
              <div class="text p-4 d-block">
                <div class="meta mb-3">
                  <div><a href="/blog/{{$value->name_url}}"><?php echo date('d F Y',strtotime(substr($value->post_date, 0,10)));?></a></div>
                  <div><a href="/blog/{{$value->name_url}}">{{$value->name}}</a></div>
                  <div><a href="/blog/{{$value->name_url}}" class="meta-chat"><span class="icon-chat"></span> {{$value->comment_count}}</a></div>
                </div>
                <h3 class="heading"><a href="/blog/{{$value->name_url}}">{{$value->post_title}}</a></h3>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <div class="row mt-5">
          <div class="col text-center">
            <div class="block-27 justify-content-center">
              {{$artikel->links()}}
            </div>
          </div>
        </div>
      </div>
    </section>
    @endsection