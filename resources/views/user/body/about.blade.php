@extends('user.layout.app')
@section('content')
<link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
<section class="ftco-section bg-light">
    	<div class="container">
    		<div class="row d-md-flex">
	    		<div class="col-md-6 ftco-animate img about-image" style="background-image: url({{asset('uploads/imagehd/thumbnail/logo_icon_wedangcode.png') }});">
	    		</div>
	    		<div class="col-md-6 ftco-animate p-md-5">
		    		<div class="row">
		          <div class="col-md-12 nav-link-wrap mb-5">
		            <div class="nav ftco-animate nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
		              <a class="nav-link active" id="v-pills-whatwedo-tab" data-toggle="pill" href="#v-pills-whatwedo" role="tab" aria-controls="v-pills-whatwedo" aria-selected="true">Tentang kami</a>

		              <a class="nav-link" id="v-pills-mission-tab" data-toggle="pill" href="#v-pills-mission" role="tab" aria-controls="v-pills-mission" aria-selected="false">Misi kami</a>

		              <a class="nav-link" id="v-pills-goal-tab" data-toggle="pill" href="#v-pills-goal" role="tab" aria-controls="v-pills-goal" aria-selected="false">Tujuan kami</a>
		            </div>
		          </div>
		          <div class="col-md-12 d-flex align-items-center">
		            
		            <div class="tab-content ftco-animate" id="v-pills-tabContent">

		              <div class="tab-pane fade show active" id="v-pills-whatwedo" role="tabpanel" aria-labelledby="v-pills-whatwedo-tab">
		              	<div>
			                <h2 class="mb-4">Kenali kami</h2>
			              	<p>Wedangcode adalah sebuah perusahaan yang bergerak di bidang multimedia dan software development kami berdiri sudah sekitar dua tahun, kemudahan dalam akses dan komunikasi menjadi senjata kami untuk bersaing di era industri 4.0</p>

				            </div>
		              </div>

		              <div class="tab-pane fade" id="v-pills-mission" role="tabpanel" aria-labelledby="v-pills-mission-tab">
		                <div>
			                <h2 class="mb-4">Misi utama kami</h2>
			              	<p>Misi kami dalam setiap projek yang kami kerjakan adalah menyesuaikan kebutuhan konsumen dan mempersingkat waktu development sehingga tidak ada sekat antara customer dan produsen.</p>
				            </div>
		              </div>

		              <div class="tab-pane fade" id="v-pills-goal" role="tabpanel" aria-labelledby="v-pills-goal-tab">
		                <div>
			                <h2 class="mb-4">Tujuan kami kedepan</h2>
			              	<p>Selalau melakukan peningkatan kualitas dan selalu memberikan yang terbaik untuk customer, bagi kami customer adalah nomor satu dan karyawan adalah hal terpenting selanjutnya</p>
			                <p>Memberiikan yang terbaik untuk karyawan dan client adalah hal utama yang harus dipenuhi</p>
				            </div>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
    	</div>
    </section>
    @endsection