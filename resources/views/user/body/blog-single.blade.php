@extends('user.layout.single')
@section('content')
<link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ftco-animate">
            <h2 class="mb-3">{{$artikel->post_title}}</h2>
            <?php echo htmlspecialchars_decode($artikel->post_content) ?>
            <div class="tag-widget post-tag-container mb-5 mt-5">
              <div class="tagcloud">
                @foreach($tag as $result)
                <a href="/blog/tag/{{$result->slug}}" class="tag-cloud-link">{{$result->slug}}</a>
                @endforeach
              </div>
            </div>


            <div class="pt-5 mt-5">
              <h3 class="mb-5">{{$artikel->comment_count}} Comments</h3>
              <ul class="comment-list">
                @if(!empty($comment))
                @foreach($comment as $field)
                @if($field->answer == 0)
                <li class="comment">
                  <div class="vcard bio">
                    <img src="https://img.icons8.com/cute-clipart/64/000000/anime-emoji.png"  alt="gambar komentar wedangcode"/>
                  </div>
                  <div class="comment-body">
                    <h3><?php echo htmlspecialchars_decode($field->name)?></h3>
                    <div class="meta"><?php echo date('F d Y, H:i A',strtotime($field->tgl));?></div>
                    <p> <?php   ($field->isi)?></p>
                    <!-- <p><a href="#" class="reply">Reply</a></p> -->
                  </div>
                </li>
                @endif
                  @if($field->answer > 0)
                  <ul class="children">
                    <li class="comment">
                      <div class="vcard bio">
                        <img src="https://img.icons8.com/offices/40/000000/anime-emoji.png"  alt="gambar komentar wedangcode"/>
                      </div>
                      <div class="comment-body">
                        <h3><?php echo htmlspecialchars_decode($field->name)?></h3>
                        <div class="meta"><?php echo date('F Y H m',strtotime($field->tgl));?></div>
                        <p> <?php echo htmlspecialchars_decode($field->isi)?> </p>
                        <!-- <p><a href="#" class="reply">Reply</a></p> -->
                      </div>
                    </li>
                  </ul>
                  @endif
                </li>
                @endforeach
                @endif

              </ul>
              <!-- END comment-list -->
              
              <div class="comment-form-wrap pt-5">
                <h3 class="mb-5">Tinggalkan komentar</h3>
                <form action="/komentar" class="p-5 bg-light" method="post">
                  {{csrf_field()}}
                  <input type="hidden" name="comment" value="{{$artikel->comment_count}}">
                  <input type="hidden" name="id" value="{{$artikel->ID}}" >
                  <input type="hidden" name="url" value="{{$name_url}}" >
                  <div class="form-group">
                    <label for="name">Name *</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name">
                      @error('name')
                      <small class="mt-2 mb-2">{{ $message }}</small>
                     @enderror
                  </div>
                  <div class="form-group">
                    <label for="email">Email *</label>
                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email">
                      @error('email')
                      <small class="mt-2 mb-2">{{ $message }}</small>
                     @enderror
                  </div>
                  <div class="form-group">
                    <label for="message">Message</label>
                    <textarea name="message" id="message" cols="30" rows="10" class="form-control @error('message') is-invalid @enderror"></textarea>
                      @error('message')
                      <small class="mt-2 mb-2">{{ $message }}</small>
                     @enderror
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary @error('message') is-invalid @enderror">
                  </div>

                </form>
              </div>
            </div>

          </div> <!-- .col-md-8 -->
          <div class="col-md-4 sidebar ftco-animate">
            <div class="sidebar-box">
              <form action="/search" class="search-form" method="get">
                <div class="form-group">
                  <span class="icon fa fa-search"></span>
                  <input type="text" id="search" name="keyword" class="form-control" placeholder="Type a keyword and hit enter">
                </div>
              </form>
            </div>
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Categories</h3>
                @foreach($category as $num)
                <li><a href="/blog/kategori/{{$num->name}}">{{$num->name}} <span>({{$num->data_category}})</span></a></li>
                @endforeach
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
              <h3>Recent Blog</h3>
              @foreach($data_artikel as $data)
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style='background-image: url({{ asset("uploads/imagehd/thumbnail/$data->thumbnail")}})'></a>
                <div class="text">
                  <h3 class="heading"><a href="/blog/{{$data->url}}">{{$data->post_title}}</a></h3>
                  <div class="meta">
                    <div><a href="/blog/{{$data->url}}"><span class="icon-calendar"></span><?php echo date('F Y',strtotime(substr($data->post_date, 0,10)));?></a></div>
                    <div><a href="/blog/{{$data->url}}"><span class="icon-person"></span> {{$data->name}}</a></div>
                    <div><a href="/blog/{{$data->url}}"><span class="icon-chat"></span> {{$data->comment_count}}</a></div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>

        </div>
      </div>
    </section>
    @endsection <!-- .section -->