 @extends('user.layout.app')
@section('content')
<link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
 <section class="ftco-section contact-section ftco-degree-bg">
      <div class="container">
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
            <form action="/message_post" method="post">
               {{csrf_field()}}
              <div class="form-group">
                <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Your Name" name="name">
                @error('name')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
              <div class="form-group">
                <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Your Email" name="email">
                 @error('email')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Subject" name="subject">
              </div>
              <div class="form-group">
                <textarea id="" cols="30" rows="7" class="form-control @error('message') is-invalid @enderror" placeholder="Message" name="message"></textarea>
                 @error('message')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
              <div class="form-group">
                <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
              </div>
            </form>
          
          </div>

          <div class="col-md-6" id="map"></div>
        </div>
      </div>
    </section>
    @endsection