<!DOCTYPE html>
<html lang="en">
  <head>
    <title>wedangcode</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('uploads/asset/4.png')}}" style="width: 80px;">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('frontend/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/animate.css') }}">
    
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/jquery.timepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('autocomplete/easy-autocomplete.min.css') }}">
       <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
     <section class="ftco-section contact-section ftco-degree-bg">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 text-center heading-section ftco-animate">
            <span class="subheading">Pendaftaran</span>
            <h2 class="mb-2">Masukan data sesuai ketentuan</h2>
          </div>
        </div>
        <div class="row block-9 justify-content-center">
          <div class="col-md-6 pr-md-5">
            <form action="/re_register" method="post">
               {{csrf_field()}}
              <div class="form-group">
                <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Username" name="name" autocomplete="off" value="{{$name}}">
                @error('name')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
              <div class="form-group">
                <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" autocomplete="off" value="{{$email}}">
                 @error('email')
                  <small class="mt-2 mb-2">{{ $message }}</small>
                 @enderror
              </div>
               <div class="form-group">
                    <input id="password-field" type="password" class="form-control" name="password" placeholder="Password">
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    <small class="mt-2 mb-2 text-danger" id="message-error-password"></small>
                </div>
                <div class="form-group">
                    <input id="password-confirm-field" type="password" class="form-control" name="confirmation" placeholder="Confirm password">
                    <span toggle="#password-confirm-field" class="fa fa-fw fa-eye field-icon toggle-confirm-password"></span>
                    <small class="mt-2 mb-2 text-danger" id="message-error-confirm-password"></small>
                </div>
              <div class="form-group">
                <input type="submit" value="Kirim" class="btn btn-primary py-3 px-5 disabled" id="btn-send" disabled>
              </div>
            </form>
          
          </div>
        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">WedangCode.</h2>
              <p>Perusahaan yang bergerak dalam bidang teknologi,multimedia dan intern management.</p>
              <p class="mt-4"><a href="#" class="btn btn-primary p-3">Detail kami</a></p>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Layanan</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Multimedia</a></li>
                <li><a href="#" class="py-2 d-block">WEB Development</a></li>
                <li><a href="#" class="py-2 d-block">Payroll & HR</a></li>
                <li><a href="#" class="py-2 d-block">Data Entry</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
                <li><a href="/" class="py-2 d-block">Home</a></li>
                <li><a href="/about" class="py-2 d-block">About</a></li>
                <li><a href="/blog" class="py-2 d-block">Blog</a></li>
                <li><a href="/contact" class="py-2 d-block">Contact</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<div class="block-23 mb-3">
	              <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">Sigaluh,Banjarnegara</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+62 8566 4221 560</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">wedangcode.team@gmail.com</span></a></li>
	                <li><span class="icon icon-clock-o"></span><span class="text">Senin &mdash; Sabtu 8:00am - 5:00pm</span></li>
	              </ul>
	            </div>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://web.facebook.com/wedangcode.wedangcode.3"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/wedang.code/"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | wedangcode <i class="icon-heart" aria-hidden="true"></i>  <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#44ffcc"/></svg></div>


  <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('frontend/js/popper.min.js') }}"></script>
  <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('frontend/js/aos.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ asset('frontend/js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.timepicker.min.js') }}"></script>
  <script src="{{ asset('frontend/js/scrollax.min.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{ asset('frontend/js/google-map.js') }}"></script>
  <script src="{{ asset('frontend/notfy/bootstrap-notify.min.js') }}"></script>
  <script src="{{ asset('autocomplete/jquery.easy-autocomplete.min.js') }}"></script>
  <script src="{{ asset('frontend/js/main.js') }}"></script>
  <script src="{{ asset('frontend/js/custom.js') }}"></script>
  <script type="text/javascript">
$(document).ready(function() {
    
    $('.button-psswd').on('click', function() {
        
        if ($('.password').attr('psswd-shown') == 'false') {
            
            $('.password').removeAttr('type');
            $('.password').attr('type', 'text');
            
            $('.password').removeAttr('psswd-shown');
            $('.password').attr('psswd-shown', 'true');
            
            $('.button-psswd').html('Hide password');
            
        }else {
            
            $('.password').removeAttr('type');
            $('.password').attr('type', 'password');
            
            $('.password').removeAttr('psswd-shown');
            $('.password').attr('psswd-shown', 'false');
            
            $('.button-psswd').html('Show password');
            
        }
        
    });
    
});
  </script>
  </body>
</html>
    