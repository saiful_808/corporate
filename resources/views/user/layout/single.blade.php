<!DOCTYPE html>
<html lang="en">
  <head>
    <title>{{$artikel->post_title}}</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> 
	<meta name="author" content="saiful">
	<meta name="description" content="{{$artikel->meta_value}}" /> 
	<link rel="canonical" href="https://www.wedangcode.com/blog/{{$name_url}}" /> 
	<meta property="og:locale" content="id_id" /> 
	<meta property="og:type" content="article" /> 
	<meta property="og:title" content="{{$artikel->post_title}}" /> 
	<meta property="og:description" content="{{$artikel->meta_value}}" /> 
	<meta property="og:url" content="https://www.wedangcode.com/blog/{{$name_url}}" /> 
	<meta property="og:site_name" content="wedangcode" /> 
	<meta property="article:publisher" content="https://web.facebook.com/wedangcode.wedangcode.3" /> 
	<meta property="article:section" content="Ajax" /> 
	<meta property="og:image" content='{{ asset("uploads/imagehd/thumbnail/$artikel->thumbnail")}}' /> 
	<meta property="og:image:width" content="460" /> 
	<meta property="og:image:height" content="440" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('uploads/asset/4.png')}}" style="width: 80px;">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('frontend/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/animate.css') }}">
    
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/jquery.timepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('autocomplete/easy-autocomplete.min.css') }}">
  </head>
  <body>
    
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="/">WedangCode.</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span>
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a href="/" class="nav-link">Home</a></li>
          <li class="nav-item {{ Request::is('about') ? 'active' : '' }}"><a href="/about" class="nav-link">About</a></li>
          <li class="nav-item {{ Request::is('blog') ? 'active' : '' }}"><a href="/blog" class="nav-link">Blog</a></li>
          <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}"><a href="/contact" class="nav-link">Contact</a></li>
          <li class="nav-item cta"><a href="/join_us" class="nav-link"><span>Get started</span></a></li>
        </ul>
      </div>
    </div>
  </nav>
    <!-- END nav -->
    @yield('content')

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">WedangCode.</h2>
              <p>Perusahaan yang bergerak dalam bidang teknologi,multimedia dan intern management.</p>
              <p class="mt-4"><a href="/about" class="btn btn-primary p-3">Detail kami</a></p>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Layanan</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Multimedia</a></li>
                <li><a href="#" class="py-2 d-block">WEB Development</a></li>
                <li><a href="#" class="py-2 d-block">Payroll & HR</a></li>
                <li><a href="#" class="py-2 d-block">Data Entry</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
                <li><a href="/" class="py-2 d-block">Home</a></li>
                <li><a href="/about" class="py-2 d-block">About</a></li>
                <li><a href="/blog" class="py-2 d-block">Blog</a></li>
                <li><a href="/contact" class="py-2 d-block">Contact</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<div class="block-23 mb-3">
	              <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">Sigaluh,Banjarnegara</span></li>
	                <li><a href="callto:085664221560"><span class="icon icon-phone"></span><span class="text">+62 8566 4221 560</span></a></li>
	                <li><a href="mailto:wedangcode.team@gmail.com"><span class="icon icon-envelope"></span><span class="text">wedangcode.team</span></a></li>
	                <li><span class="icon icon-clock-o"></span><span class="text">Senin &mdash; Sabtu 8:00am - 5:00pm</span></li>
	              </ul>
	            </div>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://web.facebook.com/wedangcode.wedangcode.3"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/wedang.code/"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | wedangcode <i class="icon-heart" aria-hidden="true"></i>  <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#44ffcc"/></svg></div>


  <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('frontend/js/popper.min.js') }}"></script>
  <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('frontend/js/aos.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ asset('frontend/js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('frontend/js/jquery.timepicker.min.js') }}"></script>
  <script src="{{ asset('frontend/js/scrollax.min.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{ asset('frontend/js/google-map.js') }}"></script>
  <script src="{{ asset('frontend/notfy/bootstrap-notify.min.js') }}"></script>
  <script src="{{ asset('autocomplete/jquery.easy-autocomplete.min.js') }}"></script>
  <script src="{{ asset('frontend/js/main.js') }}"></script>
  <script src="{{ asset('frontend/js/custom.js') }}"></script>
   @if ($message = Session::get('success'))
  <script type="text/javascript">
    $.notify({
      icon: "https://img.icons8.com/office/40/000000/new-message.png",
      message: "{{ $message }}"
    },{
      icon_type: 'image'
    });
  </script>
   @endif
  <script type="text/javascript">
    $('.datepicker').datepicker();
  </script>
  </body>
</html>
