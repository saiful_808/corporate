     $("#search").on('keyup',function(){
      $value = $(this).val();
      $.ajax({
        type : "get",
        url  : "www.wedangcode.com/search",
        data : {'search':$value},
        success:function(result){
         var options = {
            data: result,
            list: {
              maxNumberOfElements: 5,
              match: {
                enabled: true
              }
            }
          };
          $("#search").easyAutocomplete(options);
        }
      })
     });
     $("#password-confirm-field").on("keyup",function(){
        $value = $(this).val();
        data = $('#password-field').val();
        var len = data.length;
        
        if(len < 1) {
          $("#password-field").attr("class", "form-control is-invalid ");
          $("#message-error-password").html("Password tidak boleh kosong");
            // Prevent form submission
            event.preventDefault();
        }
         
        if($('#password-field').val() != $value) {
            $("#password-confirm-field").attr("class", "form-control is-invalid ");
            $("#message-error-confirm-password").html("Password tidak cocok")
            // Prevent form submission
            event.preventDefault();
        }else{
          $("#password-confirm-field").attr("class", "form-control");
          $("#message-error-confirm-password").html("");
          $("#message-error-password").html("");
          $("#btn-send").removeAttr("disabled");
        }

     });
      $(document).ready(function(){
        $('#kategori').change(function(){
            var id=$(this).val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url : "http://localhost:8000/get_sub_service",
                method : "POST",
                data : {id: id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                  if (data.length > 0) {
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
                    }
                  }else{
                        html += '<option>-- Maaf pelayanan belum siap --</option>';
                  }
                   $('.subkategori').html(html);
                     
                }
            });
        });
    });

$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
$(".toggle-confirm-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
    $('.datepicker').datepicker();