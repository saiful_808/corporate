<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','W_default@index');
Route::get('/about','W_default@about');
Route::get('/contact','W_default@contact');
Route::get('/blog','W_default@blog');
Route::get('/service/{id}','W_default@service');
Route::post('/get_sub_service','W_default@get_sub_service');
Route::get('/join_us','W_default@register');
Route::get('/join_us/{id}','W_default@register_next');
Route::get('/search','W_default@search');
Route::get('/blog/{id}','W_default@preview');
Route::get('/blog/kategori/{id}','W_default@category');
Route::get('/blog/tag/{id}','W_default@tag');
Route::post('/create_contract','W_default@create_contract');
Route::post('/email_post','W_default@email_post');
Route::post('/re_register','W_default@re_register');
Route::post('/message_post','W_default@message_post');
Route::post('/komentar','W_default@komentar_post');