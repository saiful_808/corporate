<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class message extends Model
{
    protected $table ='message';
    protected $primaryKey = 'id';
}
