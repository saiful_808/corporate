<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class meta extends Model
{
    protected $table ='meta_page';
    protected $primaryKey = 'id';
}
