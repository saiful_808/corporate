<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class comments extends Model
{
    protected $table ='wp_comments';
    protected $primaryKey = 'id';
}
