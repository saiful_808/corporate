<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Company;
use \App\User;
use \App\Comment;
use \App\models\comments;
use \App\models\meta;
use \App\models\contract;
use \App\models\email;
use \App\models\message;
use Illuminate\Support\Str;
use Mail; 
use DB;
use Session;

class W_default extends Controller
{

    public function index()
    {
        $data_meta   = meta::find(2);
        $dataservice = Company::all();
        $datacomment = Comment::all();
        return view("user.body.index",['data_service'=>$dataservice,'data_comment' => $datacomment,"data_meta" => $data_meta]);
    }
    public function email_post(Request $request)
    {
    	$email = new email();
    	$email->email = request('email');
    	$email->save();
    	return redirect("/");
    }
    public function message_post(Request $request)
    {
        $validatedData = $request->validate([   
        'message' => 'required',
        'name'    => 'required',
        'email'   => 'required'
         ]);
        $message = new message();
        $message->name = request('name');
        $message->email = request('email');
        $message->subject = request('subject');
        $message->message = request('message');
        $message->save();
        Session::flash('alert-success', 'success');
        return redirect("/contact")->with('success','Pesan berhasil dikirirm');
    }

    public function about()
    {
       $data_meta   = meta::find(1);
       return view("user.body.about",["data_meta" => $data_meta]);
    }
    public function contact()
    {
       $data_meta   = meta::find(4);
       return view("user.body.contact",["data_meta" => $data_meta]);
    }
     public function blog()
    {
        $data_meta   = meta::find(3);
        $artikel = DB::table('article')
                        ->select("article.*","wp_users.display_name as name","wp_alias.url as name_url")
                        ->where("post_status","=","publish")
                        ->join("wp_users","article.post_author",'=','wp_users.id')
                        ->join("wp_alias","article.alias_id",'=','wp_alias.id')
                        ->orderby("article.post_date","desc")
                        ->paginate(6);
       return view("user.body.blog",['artikel' => $artikel,"data_meta" => $data_meta]);
    }
    public function register()
    {
        $data_meta   = meta::find(5);
        $dataservice   = Company::all();
        $data_contract = DB::table('tipe_contract')
                            ->get();
        return view("user.body.register",["data_service" => $dataservice,"data_contract" => $data_contract,"data_meta" => $data_meta]);
    }
    public function preview($data_url)
    {
     $num       = DB::table('wp_alias')
                    ->select("article.id")
                    ->where('wp_alias.url','=',"$data_url")
                    ->join("article","wp_alias.id","=","article.alias_id")
                    ->get();
     if (count($num) > 0) {
     $id          = $num[0]->id;
     $tag     = DB::table('wp_term_relationships')
                ->select("wp_terms.slug")
                ->where("wp_term_relationships.object_id","=",$id)
                ->where("wp_term_taxonomy.taxonomy","!=","category")
                ->join("wp_term_taxonomy","wp_term_relationships.term_taxonomy_id",'=','wp_term_taxonomy.term_taxonomy_id')
                ->join("wp_terms","wp_term_relationships.term_taxonomy_id",'=','wp_terms.term_id')
                ->get();
     $comment  = DB::table('wp_comments')
                ->select("wp_comments.comment_author as name","wp_comments.comment_date as tgl","wp_comments.comment_content as isi","wp_comments.comment_parent as answer")
                ->where("wp_comments.comment_post_ID","=",$id)
                ->where("wp_comments.comment_approved","=",1)
                ->get();
     $artikel = DB::table('article')
                ->select("article.*","wp_users.display_name as name","wp_postmeta.meta_value")
                ->where("article.ID","=",$id)
                ->join("wp_users","article.post_author",'=','wp_users.id')
                ->join("wp_postmeta","article.id","=","wp_postmeta.post_id")
                ->orderby("article.post_date","desc")
                ->get();
     $data   = DB::table('article')
                ->select("article.*","wp_users.display_name as name","wp_alias.url as url")
                ->where("post_status","=","publish")
                ->join("wp_users","article.post_author",'=','wp_users.id')
                ->join("wp_alias","wp_alias.id","=","article.alias_id")
                ->orderby("article.post_date","desc")
                ->limit(3)
                ->get();
     $category = DB::table('wp_category')
                ->select("wp_category.name","wp_category.id",DB::raw("COUNT(category_id) as data_category"))
                ->join("article","wp_category.id","=","article.category_id")
                ->groupBy("article.category_id")
                ->get();
       return view("user.body.blog-single",['artikel' => $artikel[0],"tag" => $tag,"comment" => $comment,"data_artikel" => $data,"category" => $category,"name_url" => $data_url]);
   }else{
    return redirect("/blog");
   }
    }
       public function komentar_post(Request $request)
    {
        $validatedData = $request->validate([
        'message' => 'required',
        'name'    => 'required',
        'email'   => 'required'
         ]);
        $id = request('id');
        $url = request('url');
        $message = new comments();
        $message->comment_post_ID = $id;
        $message->comment_author = request('name');
        $message->comment_author_email = request('email');
        $message->comment_content = request('message');
        $message->comment_approved = 0;
        $message->save();
        $email = new email();
        $email->email = request('email');
        $email->save();
        $data_count = request('comment') + 1;
        DB::table('article')
            ->where('ID','=',$id)
            ->update(['comment_count' => $data_count]);
        return redirect("/blog/$url")->with('success','Komentar berhasil dikirim, Tunggu Di moderasi');;
    }
    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = [];
            $result = DB::table("article")
                        ->select("post_title")
                        ->where("post_title","LIKE","%".$request->search."%")
                        ->get();
            if ($result) {
                foreach ($result as $value) {
                    array_push($output,"$value->post_title");
                };
            };
            // return var_dump($output);
            return Response($output);
        }else{
        $field = $request->all();
        if(count($field) > 0) {
        $artikel = DB::table('article')
                        ->select("article.*","wp_users.display_name as name","wp_alias.url as name_url")
                        ->where("post_status","=","publish")
                        ->where("post_title","LIKE","%".$request->keyword."%")
                        ->join("wp_users","article.post_author",'=','wp_users.id')
                        ->join("wp_alias","article.alias_id",'=','wp_alias.id')
                        ->orderby("article.post_date","desc")
                        ->get();
        return view("user.body.result",['artikel' => $artikel]);
        }else{
            return redirect("/blog");
        };

    }
}
   public function category($id)
    {
        $data_meta   = meta::find(3);
        $key     = DB::table('wp_category')
                        ->select('id')
                        ->where('name',"=",$id)
                        ->get();
        $artikel = DB::table('article')
                        ->select("article.*","wp_users.display_name as name","wp_alias.url as name_url")
                        ->where("post_status","=","publish")
                        ->where("category_id","=",$key['0']->id)
                        ->join("wp_users","article.post_author",'=','wp_users.id')
                        ->join("wp_alias","article.alias_id",'=','wp_alias.id')
                        ->orderby("article.post_date","desc")
                        ->paginate(6);
       return view("user.body.kategori",['artikel' => $artikel,"kategori" => $id,"data_meta" => $data_meta]);
    }
    public function create_contract(Request $request)
    {
         $validatedData = $request->validate([
        'detail'  => 'required',
        'name'    => 'required',
        'email'   => 'required',
        'number'  => 'required',
        'service' => 'required',
        'contract' => 'required',
        'sub_service' => 'required',
         ]);
        $contract_number = "REF|".request('service')."|".request('sub_service')."|".date("y")."|".date("d")."|".date("m")."-".Str::random(2);
        $contract = new contract();
        $contract->name = request('name');
        $contract->email = request('email');
        $contract->number_phone = request('number');
        $contract->dateline = request('date');
        $contract->service_id = request('service');
        $contract->detail = request('detail');
        $contract->contract_number = $contract_number;
        $contract->id_contract = request('contract');
        $contract->sub_service_id = request('sub_service');
        $contract->save();
        
        $key     = DB::table('service')
                        ->select('name')
                        ->where('id',"=",request('service'))
                        ->get();
        $service_name = $key[0]->name;
        $key_sub     = DB::table('sub_service')
                        ->select('name')
                        ->where('id',"=",request('sub_service'))
                        ->get();
        $sub_service_name = $key_sub[0]->name;
        $key_contract     = DB::table('tipe_contract')
                        ->select('name')
                        ->where('id',"=",request('contract'))
                        ->get();
        $sub_contract = $key_contract[0]->name;
        // return view("user.mail.email",["name" => request('name'),"date" => date("d F yy"),"service_name" => $service_name,"sub_service" => $sub_service_name,"contract_name" => $sub_contract,"contract_number" => $contract_number]);
        try{
                Mail::send('user.mail.email',["name" => request('name'),"date" => date("d F yy"),"service_name" => $service_name,"sub_service" => $sub_service_name,"contract_name" => $sub_contract,"contract_number" => $contract_number], function ($message) use ($request)
                {
                    $message->subject("Contract Success");
                    $message->from('wedangcode.team@gmail.com', 'wedangcode');
                    $message->to($request->email);
                });
                return redirect("/join_us")->with('success','Silahkan cek E-mail anda, untuk Konfirmasi');
            }
            catch (Exception $e){
                return redirect("/join_us")->with('success',$e->getMessage());
            }
        
    }
    public function get_sub_service(Request $request){
        $id = request('id');
        $data  = DB::table("sub_service")
                    ->where("id_service","=",$id)
                    ->get();
        return Response($data);
    }
        public function register_next($id)
    {
        $cek = DB::table("client_new")
                ->select("email","name")
                ->where("contract_number","=","$id")
                ->get();
        if (count($cek) > 0) {
            return view("user.user_log.sign_up",["email" => $cek[0]->email,"name" => $cek[0]->name]);
        }else{
            return redirect("/");
        }
    }
    public function re_register(Request $request)
    {
         $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|min:4|email|unique:users',
            'password' => 'required',
            'confirmation' => 'required|same:password',
        ]);

        $data =  new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->save();
        return redirect("/")->with('success','Berhasil, tunggu proses selanjutnya');
    }
     public function tag($id)
    {
        $data_meta   = meta::find(3);
        $key     = DB::table('wp_terms')
                        ->select('term_id')
                        ->where('slug',"=",$id)
                        ->get();
        $id_key  = $key[0]->term_id;
        $tag     = DB::table('wp_term_relationships')
                    ->select("wp_term_relationships.object_id")
                    ->where("wp_term_relationships.term_taxonomy_id","=",$id_key)
                    ->get();
        $tag_key = $tag[0]->object_id;
        $artikel = DB::table('article')
                        ->select("article.*","wp_users.display_name as name","wp_alias.url as name_url")
                        ->where("post_status","=","publish")
                        ->where("article.ID","=",$tag_key)
                        ->join("wp_users","article.post_author",'=','wp_users.id')
                        ->join("wp_alias","article.alias_id",'=','wp_alias.id')
                        ->orderby("article.post_date","desc")
                        ->paginate(6);
       return view("user.body.tag",['artikel' => $artikel,"data_meta" => $data_meta]);
    }
    public function service($data){
        $data_meta   = meta::find(3);
        $id_key = DB::table("service")
                ->select("id")
                ->where("name","=","$data")
                ->get();
        $id = $id_key[0]->id;
        $data_service = DB::table("page_service")
                            ->select("page_service.*","service.name as title","page_service.image as image")
                            ->where("id_service","=",$id)
                            ->join("service","page_service.id_service","=","service.id")
                            ->get();
        return view("user.body.product",["data_meta" => $data_meta,"data_service" => $data_service[0]]);
    }
}
